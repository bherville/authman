#Users
admin = User.new  :password => rand(36**12).to_s(36),
                  :email    => 'admin@example.com',
                  :fname    => 'Admin',
                  :lname    => 'User'

admin.roles << :admin

admin.skip_confirmation!
admin.save


User.all.each do |u|
  unless u.has_role?(:user)
    u.roles << :user
    u.save
  end
end


puts
puts
puts '---------------------------------------'
puts '|          Admin Credentials          |'
puts '---------------------------------------'
puts "Admin Email: #{admin.email}"
puts "Admin Password: #{admin.password}"
puts '---------------------------------------'
puts
puts