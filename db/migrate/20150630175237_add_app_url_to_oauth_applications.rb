class AddAppUrlToOauthApplications < ActiveRecord::Migration
  def change
    add_column :oauth_applications, :url, :string
  end
end
