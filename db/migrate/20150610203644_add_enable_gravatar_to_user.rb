class AddEnableGravatarToUser < ActiveRecord::Migration
  def change
    add_column :users, :enable_gravatar, :boolean, :default => true
  end
end
