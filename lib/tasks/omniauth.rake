namespace :omniauth do
  namespace :users do
    desc 'Export users by authorized application'
    task :export => :environment do
      users = User.all

      users_hash = Hash.new
      users.each do |u|
        users_hash[u.email] = {
            :uid          => u.id,
            :provider     => :auth_man,
            :applications => u.applications.map { |a| a.name }
        }
      end

      puts JSON.pretty_generate(users_hash)
    end
  end
end
