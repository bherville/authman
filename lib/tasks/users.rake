namespace :users do
  desc 'Setup existing users GAuth Secret'
  task setup_gauth: :environment do
    User.where(:gauth_secret => nil).find_each do |user|
      puts "Creating GAuth for #{user.email}"

      user.send(:assign_auth_secret)
      user.save!
    end
  end
end
