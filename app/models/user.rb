class User < ActiveRecord::Base
	attr_accessor :gauth_token
  include Gravtastic
  gravtastic :secure => (AppConfig.get(%w(user_profiles secure), APP_CONFIG) ? AppConfig.get(%w(user_profiles secure), APP_CONFIG) : false)

  devise :google_authenticatable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable, :omniauthable

  has_many :user_applications
  has_many :applications, :through => :user_applications

  include RoleModel
  roles :admin, :user

  validates :fname, :presence => true
  validates :lname, :presence => true

  before_save :set_roles
  before_create :set_time_zone

  ROLE_DESCRIPTIONS = {
      :admin  => 'Can create/edit/delete users.',
      :user  => 'Basic user.'
  }

  def full_name
    "#{self.fname} #{self.lname}"
  end

  def full_name_comma
    "#{self.lname}, #{self.fname}"
  end

  def only_if_unconfirmed
    pending_any_confirmation {yield}
  end

  def attempt_set_password(params)
    p = {}
    p[:password] = params[:password]
    p[:password_confirmation] = params[:password_confirmation]
    update_attributes(p)
  end

  def password_required?
    super if confirmed?
  end

  # new function to return whether a password has been set
  def has_no_password?
    self.encrypted_password.blank?
  end

  def password_match?
    self.password == self.password_confirmation
  end

  private
  def set_roles
    self.roles << :user unless self.has_role?(:user)
  end

  def set_time_zone
    self.time_zone = Time.zone.name unless self.time_zone
  end
end
