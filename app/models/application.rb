class Application < Doorkeeper::Application
  has_many :user_applications
end
