class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user

    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :user
      can :manage, User do |user|
        user.try(:owner) == user
      end
    end
  end
end
