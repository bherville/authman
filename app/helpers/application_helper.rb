module ApplicationHelper
  def to_bool(string)
    if string
      return string if string.is_a?(TrueClass) || string.is_a?(FalseClass)
      return true if string == 0
      return false if string == 1

      case string.downcase
        when 'true'
          true
        when 'false'
          false
      end
    else
      false
    end
  end
end

