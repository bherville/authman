class UsersController < ApplicationController
  load_and_authorize_resource
  before_filter :set_user

  def show
    @applications = Application.authorized_for(@user)
  end

  def edit
  end

  def update
    account_update_params = user_params

    if account_update_params[:password].blank?
      account_update_params.delete('password')
      account_update_params.delete('password_confirmation')
    end

    if @user.update(account_update_params)
      # Sign in the user by passing validation in case their password changed
      sign_in @user, :bypass => true
      flash[:notice] = t('users.updated_user')
      redirect_to root_path
    else
      render t('general.crud.edit')
    end
  end

  private
  def set_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :time_zone, :fname, :lname, :enable_gravatar)
  end
end