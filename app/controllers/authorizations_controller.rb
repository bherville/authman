class AuthorizationsController < Doorkeeper::AuthorizationsController
  before_filter :authorize!


  private
  def authorize!
    if strategy.request.client
      unless (current_resource_owner.applications.map{ |a| a.id}).include?(strategy.request.client.application.id)
        @authorize_error ="You are not authorized to access #{strategy.request.client.application.name}!"
        render :error
      end
    end
  end
end