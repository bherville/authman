class HelpController < ApplicationController
  def download_manual
    send_file Rails.root.join('docs', 'Authentication Manager.docx'), :x_sendfil => true
  end
end
