class SessionsController < Devise::SessionsController
  def create
    # If this is an OmniAuth based login and the user has successfully logged in,
    #   remember the omniauth_redirect_to as super will remove it from session
    omniauth_redirect_to = session[:omniauth_redirect_to] if session[:omniauth_redirect_to]

    super

    # If this is an OmniAuth based login and the user has successfully logged in, reset the omniauth_redirect_to
    session[:omniauth_redirect_to] = omniauth_redirect_to if omniauth_redirect_to
  end
end
