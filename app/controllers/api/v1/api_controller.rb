class API::V1::APIController < ApplicationController
  private
  def current_resource_owner
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end

  def authorized?
    (current_resource_owner.applications.map{ |a| a.id}).include?(doorkeeper_token.application.id)
  end
end
