class API::V1::CredentialsController < API::V1::APIController
  skip_before_filter :verify_authenticity_token, :only => :me
  before_action :doorkeeper_authorize!

  respond_to     :json


  def me
    respond_with current_resource_owner
  end
end
