class API::V1::ApplicationsController < ApplicationController
  load_and_authorize_resource
  before_filter :set_applications, only: [:index]

  def index
    render 'api/v1/applications/index.json.jpbuilder', :callback => params[:callback]
  end


  private
  def set_applications
    case params[:filter]
      when :all
        # Validate user is an Admin and list all the applications setup
        if current_user.has_role? :admin
          @applications = Application.all
        else
          nil
          # raise exception
        end

      else
        # Only return the users allowed applications
        @applications = current_user.applications
    end
  end
end
