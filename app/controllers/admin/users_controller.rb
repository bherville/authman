class Admin::UsersController < ApplicationController
  load_and_authorize_resource

  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def new
    @user = User.new

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def edit
    @user = User.find(params[:id])
    @applications = Doorkeeper::Application.order(:name)
  end

  def create
    user_account_params = user_params

    @user = User.new(user_account_params)

    if @user.save
      flash[:notice] = t('users.created_user')
      redirect_to admin_user_path(@user)
    else
      render :action => 'new'
    end
  end

  def update
    @user = User.find(params[:id])

    user_account_params = user_params

    if user_account_params[:password].blank?
      user_account_params.delete(:password)
      user_account_params.delete(:password_confirmation)
    end

    if @user.update_attributes(user_account_params)
      flash[:notice] = t('users.updated_user')
      redirect_to admin_users_path
    else
      render :action => 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      flash[:notice] = t('users.deleted_user')
      redirect_to admin_users_path
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :time_zone, :fname, :lname, roles: [], application_ids: [])
  end
end
