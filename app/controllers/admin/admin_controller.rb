class Admin::AdminController < ApplicationController
  load_and_authorize_resource

  def index
    @admin_pages = Hash.new

    @admin_pages[:users] = {
        :title => t('users.users'),
        :path => admin_users_path,
        :description => t('admin.manage_users')
    }

    @admin_pages[:ouath] = {
        :title => t('admin.oauth'),
        :path => oauth_applications_path,
        :description => t('admin.manage_oauth')
    }

    respond_to do |format|
      format.html
    end
  end
end
