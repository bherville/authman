class ApplicationsController < Doorkeeper::ApplicationsController
  private
  def application_params
    if params.respond_to?(:permit)
      params.require(:doorkeeper_application).permit(:name, :redirect_uri, :url)
    else
      params[:doorkeeper_application].slice(:name, :redirect_uri, :url) rescue nil
    end
  end
end
