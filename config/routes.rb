Rails.application.routes.draw do
  use_doorkeeper do
    controllers :authorizations => 'authorizations'
    controllers :applications => 'applications'
  end

  authenticated :user do
    root :to => 'users#show', :as => :authenticated_root
  end

  root :to => redirect('/users/sign_in')

  devise_for :users, :controllers => {
                       :confirmations => 'confirmations',
                       :sessions  => 'sessions',
                       :checkga => 'checkga'
                   }

  as :user do
    patch '/user/confirmation' => 'confirmations#update', :via => :patch, :as => :update_user_confirmation
  end

  resource :user, only: [:show, :edit, :update]
  resource :help, :controller => 'help' do
    get :download_manual
  end

  namespace :admin do
    get '/' => 'admin#index'
    resources :users
  end

  namespace :api, :defaults => {:format => :json} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true), :defaults => {:format => :json} do
      get '/me' => 'credentials#me'
      resources :applications
    end
  end
end
